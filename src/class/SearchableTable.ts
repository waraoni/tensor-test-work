import ITableDataRow from '@/interface/ITableDataRow';
import Table from '@class/Table';
import ITableData from '@interface/ITableData';

export class SearchableTable extends Table {
  protected tableWrapper: HTMLDivElement

  protected searchQuery: RegExp|null = null

  constructor(root: HTMLDivElement, data: ITableData) {
    super(document.createElement('TABLE') as HTMLTableElement, data); // Call the parent constructor, give it a table

    // Bind SearchableTable EventListener to the current instance
    this.eventListeners.searchQueryChangeHandler = this.searchQueryChangeHandler.bind(this) as EventListener;

    this.tableWrapper = root;
    this.tableWrapper.classList.add('searchable-table'); // Add class fro styling

    SearchableTable.prototype.render.call(this);
  }

  /**
   * Overrides Table::render
   * Renders the component
   */
  public render(): void {
    // prevent memory leaks
    this.detachSearchListeners();

    this.tableWrapper.innerHTML = ''; // Clear
    Table.prototype.render.call(this); // Call parent render function
    this.tableWrapper.appendChild(this.renderSearchInput()); // Add search input
    this.tableWrapper.appendChild(this.table); // Add the rendered table
  }

  /**
   * Renders the search input
   */
  protected renderSearchInput(): HTMLInputElement {
    const searchInput = document.createElement('INPUT') as HTMLInputElement;
    searchInput.setAttribute('placeholder', 'Search query');
    searchInput.classList.add('searchable-table__search-input');
    searchInput.addEventListener('input', this.eventListeners.searchQueryChangeHandler);
    return searchInput;
  }

  /**
   * Filters the data according to the search query
   * @param rows is the rows of table data to filter
   */
  protected getFilteredRows(rows: ITableDataRow[]): ITableDataRow[] {
    if (this.searchQuery instanceof RegExp) {
      // In case a user has entered a search query

      // First, filter the data
      const filtered = rows.filter((row) => {
        for (const column of this.data.columns) {
          if (row[column] && row[column].toString().match(this.searchQuery)) {
            return true;
          }
        }

        return false;
      });

      // Then, highlight the search query
      const highlighted = filtered.map((row) => {
        const data: ITableDataRow = {};

        for (const column of this.data.columns) {
          data[column] = row[column].replace(this.searchQuery, '<span class="searchable-table__found-text">$1</span>');
        }

        return data;
      });

      return highlighted;
    }

    // Otherwise (no search query) return unfiltered data
    return rows;
  }

  /**
   * Overrides Table::getRows method.
   * Is used when rendering rows.
   * This version of the method takes search query into account
   */
  protected getRows(): ITableDataRow[] {
    return this.getSortedRows(this.getFilteredRows(this.data.rows));
  }

  /**
   * A listener that creates a regexp out of the search query.
   * Expected to be bound to this before attaching to an HTMLElement
   */
  protected searchQueryChangeHandler(this: SearchableTable, event: KeyboardEvent): void {
    const searchInput = event.target as HTMLInputElement;
    this.searchQuery = searchInput.value
      ? new RegExp(`(${searchInput.value})`, 'ig')
      : null;

    Table.prototype.render.call(this);
  }

  /**
   * Removes EventListeners from the search input
   */
  protected detachSearchListeners(): void {
    const searchInputs = this.tableWrapper.querySelectorAll('input.searchable-table__search-input');
    for (let i = 0; i < searchInputs.length; i++) {
      searchInputs[i].removeEventListener('input', this.eventListeners.searchQueryChangeHandler);
    }
  }
}

export default SearchableTable;
