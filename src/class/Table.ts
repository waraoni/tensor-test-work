import ITableDataRow from '@/interface/ITableDataRow';
import { ITableData, isITableData } from '@interface/ITableData';

import '@style/table.styl';

export class Table {
  protected table: HTMLElement // Root table element

  protected data: ITableData = { columns: [], rows: [] } // Data

  protected sortByColumn: string|null = null // Sorting predicate

  protected sortAscending = true // Sorting direction

  protected eventListeners: { [listenerName: string]: EventListener } = {} // Event listeners bound to Table instance

  constructor(root: HTMLTableElement, data: ITableData) {
    this.table = root;
    this.table.classList.add('table');
    this.eventListeners.sortClickHandler = this.sortClickHandler.bind(this) as EventListener;

    if (isITableData(data)) {
      // If data is correct
      this.data = data;
      this.sortByColumn = data.columns.length
        ? data.columns[0]
        : null;
    } else alert('Data has incorrect format');

    // Here and on, call Table::render explicitly so that
    // the render function of
    // extending class doesn't get called
    Table.prototype.render.call(this);
  }

  /**
   * Expected to be Overrined.
   * Renders the table
   */
  render(): void {
    // prevent memory leaks
    this.detachThListeners();

    this.table.innerHTML = ''; // Clear
    this.table.appendChild(this.renderHead());
    this.table.appendChild(this.renderBody());
  }

  /**
   * Renders table head
   */
  private renderHead(): HTMLElement {
    const head = document.createElement('THEAD');
    const headRow = document.createElement('TR');

    head.appendChild(headRow);

    for (const column of this.data.columns) {
      const th = document.createElement('TH');
      th.dataset.name = column;
      th.innerHTML = column + this.renderSortingArrow(column);
      th.addEventListener('click', this.eventListeners.sortClickHandler);
      headRow.appendChild(th);
    }

    return head;
  }

  /**
   * Renders table body
   */
  private renderBody(): HTMLElement {
    const body = document.createElement('TBODY');
    for (const row of this.getRows()) {
      body.appendChild(this.renderRow(row));
    }

    return body;
  }

  /**
   * Renders a row of table body
   */
  private renderRow(rowData: ITableDataRow): HTMLElement {
    const row = document.createElement('TR');

    for (const column of this.data.columns) {
      const cell = document.createElement('TD');
      cell.innerHTML = rowData[column] || ''; // If there's no data provided for the column, render empty string
      row.appendChild(cell);
    }

    return row;
  }

  /**
   * Renders the arrow that indicates the column
   * by which the data is sorted and the
   * sorting direction.
   *
   * @param column is the column name for which we render or not the arrow
   */
  private renderSortingArrow(column: string): string {
    if (this.sortByColumn === column) {
      return this.sortAscending
        ? '<span class="table__sort-arrow">▲</span>'
        : '<span class="table__sort-arrow">▼</span>';
    }
    return '';
  }

  /**
   * Returns sorted data
   */
  protected getSortedRows(rows: ITableDataRow[]): ITableDataRow[] {
    if (this.sortByColumn === null) return rows;

    return rows
      .sort((aRow, bRow) => {
        const aValue = (aRow[this.sortByColumn!] || '');
        const bValue = (bRow[this.sortByColumn!] || '');
        return this.sortAscending
          ? aValue.localeCompare(bValue)
          : bValue.localeCompare(aValue);
      });
  }

  /**
   * Expected to be overriden, for customization.
   * Returns data for the Table::renderBody method.
   */
  protected getRows(): ITableDataRow[] {
    return this.getSortedRows(this.data.rows);
  }

  /**
   * Handles the click on table column header
   * to switch sorting.
   * Expected to be bound to this before attaching to an HTMLElement
   */
  protected sortClickHandler(this: Table, event: MouseEvent): void {
    const column = (event.target as HTMLElement);
    this.sortAscending = this.sortByColumn === column.dataset.name
      ? !this.sortAscending
      : true;
    this.sortByColumn = column.dataset.name || null;
    Table.prototype.render.call(this);
  }

  /**
   * Clears listeners attached to the
   * table's column headers
   */
  protected detachThListeners(): void {
    const th = this.table.querySelectorAll('th');
    for (let i = 0; i < th.length; i++) {
      th[i].removeEventListener('click', this.eventListeners.sortClickHandler);
    }
  }
}

export default Table;
