import SearchableTable from '@class/SearchableTable';

const tableDataJson = require('@mock/table-data.json').default; // This is the path to json data for our table

export class App {
  protected root: HTMLElement

  constructor(root: HTMLElement) {
    this.root = root;
    this.initialiazeTable();
  }

  protected async initialiazeTable() {
    try {
      const tableData = await (await fetch(tableDataJson)).json();

      const tableWrapElement = document.createElement('div') as HTMLDivElement;
      this.root.innerHTML = '';
      this.root.appendChild(tableWrapElement);

      new SearchableTable(tableWrapElement, tableData);
    } catch (error) {
      alert('Error loading data. Try refreshing the page');
    }
  }
}

export default App;
