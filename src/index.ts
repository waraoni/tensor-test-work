import App from '@class/App';

import '@style/common.styl';

(function () {
  /**
   * Runs a callback on DOMContentLoaded event
   */
  const onDOMReady = function (callback: () => void) {
    if (document.readyState !== 'loading') callback();
    else document.addEventListener('DOMContentLoaded', (callback as EventListener));
  };

  onDOMReady(() => {
    // initialize the App
    const appRootElement = document.getElementById('app');
    if (appRootElement) new App(appRootElement);
    else alert('Something went wrong: nowhere to mount the App.');
  });
}());
