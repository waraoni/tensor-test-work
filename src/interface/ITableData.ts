import ITableDataRow from '@interface/ITableDataRow';

export interface ITableData {
  columns: string[]
  rows: ITableDataRow[]
}

/**
 * Returns TRUE if an obj is ITableData
 */
export const isITableData = function (obj: any): obj is ITableData {
  return (
    obj
    && obj.columns instanceof Array
    && obj.columns.every((columnName: any) => typeof columnName === 'string')
    && obj.rows instanceof Array
  );
};

export default ITableData;
