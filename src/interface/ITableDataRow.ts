export interface ITableDataRow {
  [key: string]: any
}

export default ITableDataRow;
