const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HTMLInlineCSSWebpackPlugin = require('html-inline-css-webpack-plugin').default;

const src = path.resolve(__dirname, 'src');

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[chunkhash:8].js',
    chunkFilename: '[name].[chunkhash:8].js',
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    open: true,
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.json$/,
        exclude: /node_modules/,
        loader: 'file-loader',
        type: 'javascript/auto',
        options: {
          name: '[name].[contenthash].[ext]',
        },
      },
      {
        test: /\.styl(us)?$/,
        exclude: /node_modules/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'stylus-loader'],
      },
    ],
  },
  resolve: {
    alias: {
      '@': src,
      '@html': path.resolve(src, 'html'),
      '@style': path.resolve(src, 'style'),
      '@interface': path.resolve(src, 'interface'),
      '@class': path.resolve(src, 'class'),
      '@mock': path.resolve(src, 'mock'),
    },
    extensions: ['.ts', '.tsx', '.js', '.json'],
    modules: [
      'node_modules',
      process.cwd(),
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/html/index.html',
      scriptLoading: 'defer',
    }),
    new MiniCssExtractPlugin(),
    new HTMLInlineCSSWebpackPlugin(),
  ],
};
